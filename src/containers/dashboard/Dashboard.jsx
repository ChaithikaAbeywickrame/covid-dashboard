import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NavBar from "../../components/navbar/NavBar";
import WorldSummary from "../../components/world-summary/WorldSummary";
import SriLankanInfo from "../../components/sl-data/SriLankanInfo";
import About from "../../components/about-us/AboutUs";
import ContactUs from "../../components/contact-us/ContactUs";
//import Footer from '../../components/footer/Footer'

const Dashboard = () => {
  return (
    <div>
      <NavBar />
      <div className="body-content">
        <div className="container">
          <div className="animated fadein">
            <Redirect from="/" to="/dashboard" />
            <Switch>
              <Route path="/dashboard" exact component={WorldSummary} />
              <Route
                path="/dashboard/sri-lanka"
                exact
                component={SriLankanInfo}
              />
              <Route path="/dashboard/aboutus" exact component={About} />
              <Route path="/dashboard/contactus" exact component={ContactUs} />
            </Switch>
          </div>
        </div>
      </div>
      {/* <Footer/> */}
    </div>
  );
};

export default Dashboard;
