

import  { applyMiddleware, createStore } from "redux";
import createSagaMiddleware from 'redux-saga';
import reducers from "../reducers/index"
import logger from 'redux-logger'
import rootSaga from '../sagas/index'

const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducers,
    applyMiddleware(sagaMiddleware, logger),
);

sagaMiddleware.run(rootSaga);

export default store;