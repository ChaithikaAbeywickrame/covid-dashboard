

import { all, fork } from 'redux-saga/effects';

import coronaData from './corona-data'
import slData from './sl-data'

export default function* rootSaga() {
  yield all([fork(coronaData), fork(slData)]);
}