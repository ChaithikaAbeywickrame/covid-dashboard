import {
    put,
    takeLatest,
    all,
    call
} from 'redux-saga/effects';

import * as Api from '../api/';

import {
    FETCH_SL_DATA_FAILURE,
    FETCH_SL_DATA_REQUEST,
    FETCH_SL_DATA_SUCCESS
}
from '../actions/constants';

function* sendFetchRequest() {
    try {
        const response = yield call(Api.retrieveSlData)

        if (response.data.token !== null) {
            yield put({
                type: FETCH_SL_DATA_SUCCESS,
                payload: response.data
            });
        } else {
            yield put({
                type: FETCH_SL_DATA_FAILURE,
                error: response.data
            });
        }
    } catch (err) {
        yield put({
            type: FETCH_SL_DATA_FAILURE,
            error: err
        })
    }
}

function* actionWatcher() {
    yield takeLatest(FETCH_SL_DATA_REQUEST, sendFetchRequest);
}

export default function* rootSaga() {
    yield all([actionWatcher()]);
}