

import {
    FETCH_SL_DATA_FAILURE,
    FETCH_SL_DATA_SUCCESS,
} from '../actions/constants'

export default function slData(
    state = {
        srilankanData: {},
        slRequest: false,
    },
    action
) {
    switch (action.type) {
        case FETCH_SL_DATA_SUCCESS:
            return {
                ...state, srilankanData: action.payload, slRequest: true
            };
        case FETCH_SL_DATA_FAILURE:
            return {
                ...state, slRequest: false
            };
        default:
            return state;
    }
}