import { combineReducers } from 'redux';

import coronaData from './corona-data';
import slData from './sl-data';

export default combineReducers({
    coronaData, slData
});