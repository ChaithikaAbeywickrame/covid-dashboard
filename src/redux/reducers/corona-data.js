

import {
    FETCH_SUMMARY_DATA_SUCCESS,
    FETCH_SUMMARY_DATA_FAILURE,
    SET_SELECTED_FIG_COUNTRY,
} from '../actions/constants'

export default function coronaData(
    state = {
        summaryData: {},
        summaryRequest: false,
        selectedCountry: {}
    },
    action
) {
    switch (action.type) {
        case FETCH_SUMMARY_DATA_SUCCESS:
            return {
                ...state, summaryData: action.payload, summaryRequest: true
            };
        case FETCH_SUMMARY_DATA_FAILURE:
            return {
                ...state, summaryRequest: false
            };
        case SET_SELECTED_FIG_COUNTRY:
            return{
                ...state, selectedCountry: action.payload
            }
        default:
            return state;
    }
}