import axios from 'axios'

export const sendRequest = (
    method,
    url,
    params,
) => {
    const body = method === 'get' ? 'params' : 'data';

    const config = {
        method,
        url,
        [body]: params,
    };
    return axios.request(config);
};

// From the above pattern define your requests 

// this retrieves the summary of data
export const retrieveData = () => {
    return sendRequest('get', 'https://api.covid19api.com/summary', '')
}

// this retieves sl data from government site
export const retrieveSlData = () =>{
    return sendRequest('get', 'https://www.hpb.health.gov.lk/api/get-current-statistical', '')
}
