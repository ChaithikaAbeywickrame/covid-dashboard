

import { 
    FETCH_SUMMARY_DATA_REQUEST,
    SET_SELECTED_FIG_COUNTRY,  
} from './constants'

export const requestSummaryData = () =>{
    return { type: FETCH_SUMMARY_DATA_REQUEST}
}

export const setSelectedCountry = country => {
    return {type: SET_SELECTED_FIG_COUNTRY, payload: country}
}