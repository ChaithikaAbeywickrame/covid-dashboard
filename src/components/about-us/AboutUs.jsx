
import './AboutUs.css'
import React from 'react'
import ReactLogo from '../../assets/images/react-logo.png'
import ReduxLogo from '../../assets/images/logo-redux.png'
import ReactRouter from '../../assets/images/react-router.png'
import ReduxSaga from '../../assets/images/redux-saga-logo.png'
import AboutImage from '../../assets/images/about-image.jpg'

const AboutUs = () => {

    return (
        <div class="aboutus-wrp">
            <h1 className="page-heading">About</h1>
            <div className="row ">
                <div className="col-md-8">
                    <h3 className="section-heading">Technologies and APIs</h3>
                    <p> The technology used for this is <a href="www.reactjs.org">ReactJs</a></p>
                    <p> Along with ReactJs, the below mentioned libraries and APIs are used to add the Graphs, State Management and Routing</p>
                    <ul>
                        <li>State Management - <a href="https://redux.js.org/">Redux</a></li>
                        <li>Middleware - <a href="https://redux-saga.js.org/">Redux-saga</a></li>
                        <li>Barcharts - <a href="https://www.npmjs.com/package/react-charts">React-Charts</a></li>
                        <li>PieCharts - <a href="https://www.npmjs.com/package/react-minimal-pie-chart">React-Minimal-Pie-Chart</a></li>
                        <li>Routing - <a href="https://www.npmjs.com/package/react-router-dom">React-Router-Dom</a></li>
                        <li>Sri Lanka Data -<a href="https://www.hpb.health.gov.lk/api/get-current-statistical">endpoint</a> - Data provided by the <a href="https://www.hpb.health.gov.lk/en">Health Ministry</a> Sri Lanka</li>
                        <li>World Facts - <a href="https://api.covid19api.com/summary">endpoint</a></li>
                    </ul>
                </div>
                <div className="col-md-4">
                    <img class="about-image" src={AboutImage} alt="" srcset=""/>
                </div>
            </div>

            <div className="row img-row">
                <div className="col-md-3 img-wrp"><img class="technology-image" src={ReactLogo} alt="" srcset="" /></div>
                <div className="col-md-3 img-wrp"><img class="technology-image" src={ReduxLogo} alt="" srcset="" /></div>
                <div className="col-md-3 img-wrp"><img class="technology-image" src={ReactRouter} alt="" srcset="" /></div>
                <div className="col-md-3 img-wrp"><img class="technology-image" src={ReduxSaga} alt="" srcset="" /></div>
            </div>
        </div>
    )

}

export default AboutUs;