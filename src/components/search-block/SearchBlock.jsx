

import React from 'react'
import { useEffect } from 'react';
import { useDispatch, useSelector, shallowEqual } from 'react-redux'

import CountryFactBlock from '../country-fact-block/CountryFactBlock'
import { setSelectedCountry } from '../../redux/actions/corona-data';


const SearchBlock = props => {

    const dispatch = useDispatch()
    const { selectedCountry } = useSelector(state => ({
        selectedCountry: state.coronaData.selectedCountry,
    }), shallowEqual);

    let { countryList } = props.countries;


    useEffect(() => {
        setMaxCountry()
    }, [countryList]);

    const setMaxCountry = () => {
        const maxFigCountry = props.countries.reduce((max, country) => max.TotalConfirmed > country.TotalConfirmed ? max : country)
        dispatch(setSelectedCountry(maxFigCountry))
    }

    const handleChange = (e) => {
        const selectedCountry = props.countries.find(country => country.CountryCode === e.target.value)
        dispatch(setSelectedCountry(selectedCountry));
    }

    return (
        <div className="search-wrp">
            <div className="row">
                <div className="col-md-4">
                    <select className="custom-select" onChange={(e) => handleChange(e)}>
                        <option defaultValue>{selectedCountry.Country}</option>
                        {props.countries && props.countries.map((country, index) => (
                            <option key={index} value={country.CountryCode}>{country.Country}</option>
                        ))}
                    </select>
                </div>
            </div>
            <div className="row">
                <div className="col-md-2">
                    <CountryFactBlock color={'#dc3545'} data={selectedCountry.NewDeaths} title={'New Deaths'} />
                </div>
                <div className="col-md-2">
                    <CountryFactBlock color={'#17a2b8'} data={selectedCountry.NewConfirmed} title={'New Cases'} />
                </div>
                <div className="col-md-2">
                    <CountryFactBlock color={'#28a745'} data={selectedCountry.NewRecovered} title={'New Recovered'} />
                </div>
                <div className="col-md-2">
                    <CountryFactBlock color={'#dc3545'} data={selectedCountry.TotalDeaths} title={'Total Deaths'} />
                </div>
                <div className="col-md-2">
                    <CountryFactBlock color={'#17a2b8'} data={selectedCountry.TotalConfirmed} title={'Total Cases'} />
                </div>
                <div className="col-md-2">
                    <CountryFactBlock color={'#28a745'} data={selectedCountry.TotalRecovered} title={'Total Recovered'} />
                </div>
            </div>
        </div>
    )
}

export default SearchBlock;