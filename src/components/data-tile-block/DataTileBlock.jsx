
import './DataTileBlock.css'
import React from 'react';


const DataTileBlock = (props) => {

    let { tileTitle, tileContent, tileType } = props;
    let tileClass = (tileType) ? `itm-wrp badge-${tileType}` : `itm-wrp badge-success`;

    return (
        <div className="col-md-4 col-sm-4 col-xs-12">
            <div className={tileClass} >
                <h4>{tileTitle}</h4>
                <div className="figure-text">{tileContent}</div>
            </div>
        </div>
    )
}

export default DataTileBlock;