
import './Spinner.css';

import React from 'react';


const Spinner = () =>{
    return(
        <div className="custom-spinner text-center">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
}

export default Spinner;