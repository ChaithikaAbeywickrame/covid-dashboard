import "./WorldSummary.css";
import React, { useEffect, useState } from "react";
import { useSelector, shallowEqual } from "react-redux";

import DataTileBlock from "../data-tile-block/DataTileBlock";
import Spinner from "../spinner/Spinner";
import PieChart from "../pie-chart-block/PieChartBlock";
import BarChart from "../bar-chart-block/BarChartBlock";
import SearchBlock from "../search-block/SearchBlock";

const WorldSummary = () => {
  const { worldFacts, countryFacts } = useSelector(
    (state) => ({
      worldFacts: state.coronaData.summaryData.Global,
      countryFacts: state.coronaData.summaryData.Countries,
    }),
    shallowEqual
  );
  const [data, setData] = useState([]);

  const [countryNewCasesArray, setCountryNewCasesArray] = useState([]);
  const [countryNewDeathsArray, setCountryNewDeathsArray] = useState([]);

  const selectedCountries = [];
  const colorArray = [
    "#FF6633",
    "#FFB399",
    "#FF33FF",
    "#00B3E6",
    "#E6B333",
    "#ff6384",
    "#B34D4D",
    "#80B300",
    "#809900",
    "#E6B3B3",
    "#6680B3",
    "#66991A",
    "#FF99E6",
    "#CCFF1A",
    "#FF1A66",
    "#E6331A",
    "#33FFCC",
    "#66994D",
    "#B366CC",
    "#4D8000",
    "#B33300",
    "#CC80CC",
    "#66664D",
    "#991AFF",
    "#E666FF",
    "#4DB3FF",
    "#1AB399",
    "#E666B3",
    "#33991A",
    "#CC9999",
    "#B3B31A",
    "#00E680",
    "#4D8066",
    "#809980",
    "#E6FF80",
    "#1AFF33",
    "#999933",
    "#FF3380",
    "#CCCC00",
    "#66E64D",
    "#4D80CC",
    "#9900B3",
    "#E64D66",
    "#4DB380",
    "#FF4D4D",
    "#99E6E6",
    "#6666FF",
  ];

  const props = [
    "NewConfirmed",
    "TotalConfirmed",
    "NewDeaths",
    "TotalDeaths",
    "NewRecovered",
    "TotalRecovered",
  ];

  const [dataGenerated, setDataGenerated] = useState(false);

  useEffect(() => {
    setCountryNewCasesArray([]);
    setCountryNewDeathsArray([]);
    let count = 0;
    countryFacts &&
      countryFacts.map((country) => {
        if (country.NewConfirmed > 5000 && country.NewDeaths > 25) {
          let newCases = {
            title: country.Country,
            value: country.NewConfirmed,
            color: colorArray[count],
          };
          let newDeaths = {
            title: country.Country,
            value: country.NewDeaths,
            color: colorArray[count],
          };
          setCountryNewCasesArray((props) => [...props, newCases]);
          setCountryNewDeathsArray((props) => [...props, newDeaths]);
          selectedCountries.push(country);
          count++;
        }
      });

    generateData(selectedCountries);
  }, [countryFacts]);

  const generateData = () => {
    let newObj = {};
    selectedCountries.forEach((country) => {
      Object.keys(country).forEach((key) => {
        if (props.indexOf(key) > -1) {
          newObj[key] = newObj[key] ? newObj[key] : { label: key, data: [] };
          newObj[key].data = [
            ...newObj[key].data,
            [country["Country"], country[key]],
          ];
        }
      });
    });
    setData(Object.values(newObj));
    setDataGenerated(true);
  };

  if (worldFacts) {
    return (
      <div className="world-summary-wrp">
        <h1 className="page-heading">World Facts</h1>
        <div className="summary-cases-wrp">
          <h3 className="section-heading">Search by Country</h3>
          <div className="row">
            <div className="col-md-12">
              <SearchBlock countries={countryFacts} />
            </div>
          </div>
        </div>

        <div className="summary-cases-wrp">
          <h3 className="section-heading">Data Charts</h3>
          <div className="row">
            <div className="col-md-6">
              {<PieChart countryList={countryNewCasesArray} />}
              <h4 className="chart-heading">New Cases</h4>
            </div>
            <div className="col-md-6">
              {<PieChart countryList={countryNewDeathsArray} />}
              <h4 className="chart-heading">New Deaths</h4>
            </div>
          </div>
        </div>

        <div className="summary-cases-wrp">
          <h3 className="section-heading">Highest Cases Chart</h3>
          <div className="row">
            <div className="col-md-12">
              {dataGenerated && <BarChart data={data} />}
            </div>
          </div>
        </div>

        <div className="summary-cases-wrp">
          <h3 className="section-heading">Latest Updates</h3>
          <div className="row">
            <DataTileBlock
              tileTitle="New Cases"
              tileContent={worldFacts.NewConfirmed}
              tileType="info"
            />
            <DataTileBlock
              tileTitle="New Deaths"
              tileContent={worldFacts.NewDeaths}
              tileType="danger"
            />
            <DataTileBlock
              tileTitle="New Recovered"
              tileContent={worldFacts.NewRecovered}
              tileType="success"
            />
          </div>
        </div>
        <div className="summary-cases-wrp">
          <h3 className="section-heading">Overall Updates</h3>
          <div className="row">
            <DataTileBlock
              tileTitle="All Cases"
              tileContent={worldFacts.TotalConfirmed}
              tileType="info"
            />
            <DataTileBlock
              tileTitle="All Deaths"
              tileContent={worldFacts.TotalDeaths}
              tileType="danger"
            />
            <DataTileBlock
              tileTitle="All Recovered"
              tileContent={worldFacts.TotalRecovered}
              tileType="success"
            />
          </div>
        </div>
      </div>
    );
  }
  return <Spinner />;
};

export default WorldSummary;
