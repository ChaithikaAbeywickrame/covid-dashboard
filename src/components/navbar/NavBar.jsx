
import './NavBar.css'
import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => {

    const [buttonState, setButtonState] = useState(false);

    const [homeState, setHomeState] = useState(true)
    const [slState, setSlState] = useState(false)
    const [aboutState, setAboutState] = useState(false)
    const [contactState, setContactState] = useState(false)

    const btnClass = (buttonState) ? `navbar-toggler` : 'navbar-toggler collapsed';
    const navClass = (buttonState) ? `collapse navbar-collapse show` : 'collapse navbar-collapse';
    const navLinkClass = `nav-link`
    const navLinkActiveClass = `nav-link active`
    const handleBtnClick = () => {
        setButtonState(!buttonState)
    };

    const navLinkClick = (linkName) => {
        switch (linkName) {
            case 'sl':
                setHomeState(false)
                setSlState(true)
                setAboutState(false)
                setContactState(false)
                break
            case 'about':
                setHomeState(false)
                setSlState(false)
                setAboutState(true)
                setContactState(false)
                break
            case 'contact':
                setHomeState(false)
                setSlState(false)
                setAboutState(false)
                setContactState(true)
                break
            default:
                setHomeState(true)
                setSlState(false)
                setAboutState(false)
                setContactState(false)
                break
        }
    }

    return (

        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <div className="container">
                <Link className="navbar-brand" to="/dashboard">Covid-19 Dashboard</Link>
                <button className={btnClass} type="button" onClick={() => handleBtnClick()}>
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className={navClass} id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <Link to="/dashboard/" className={homeState ? navLinkActiveClass : navLinkClass} onClick={() => navLinkClick('home')}>Home </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/dashboard/sri-lanka/" className={slState ? navLinkActiveClass : navLinkClass} onClick={() => navLinkClick('sl')}>Sri Lanka </Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/dashboard/aboutus/" className={aboutState ? navLinkActiveClass : navLinkClass} onClick={() => navLinkClick('about')}>About</Link>
                        </li>
                        <li className="nav-item">
                            <Link to="/dashboard/contactus/" className={contactState ? navLinkActiveClass : navLinkClass} onClick={() => navLinkClick('contact')} >Contact</Link>
                        </li>
                    </ul>
                </div>
            </div>

        </nav>

    )
}

export default NavBar;