

import React, { useMemo } from 'react'
import { Chart } from 'react-charts'


const BarChartBlock = props => {
    const {data} = props;
    const series = useMemo(
        () => ({
            type: 'bar'
        }),
        []
    )
    const axes = useMemo(
        () => [
            { primary: true, type: 'ordinal', position: 'bottom' },
            { position: 'left', type: 'linear', stacked: false }
        ],
        []
    )

    return (
        < div
            style={{
                width: '100%',
                height: '600px'
            }}
        >

            <Chart data={data} series={series} axes={axes} tooltip />
        </div>
    )
}

export default React.memo(BarChartBlock);