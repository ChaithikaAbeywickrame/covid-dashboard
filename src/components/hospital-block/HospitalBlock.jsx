
import './HospitalBlock.css'

import React, { useState } from 'react'


const HospitalBlock = ({ hospitalData }) => {

    const { hospital_id, cumulative_local, cumulative_foreign, treatment_local, treatment_foreign, cumulative_total, treatment_total, hospital } = hospitalData;
    const [selectedHospital, setSelectedHospital] = useState(false);
    const btnClass = (selectedHospital) ? 'navbar-toggler collapsed hospital-btn' : 'navbar-toggler hospital-btn';
    const dataClass = (selectedHospital) ? 'collapse show' : 'collapse';

    const handleClick = () => {
        setSelectedHospital(!selectedHospital);
    }
    return (
        <div className="hospital-itm-wrp" key={hospital_id}>
            <div className="hospital-title">
                <button className={btnClass} type="button" onClick={() => handleClick()}>
                    <h5 className="hospital-name">{hospital.name}</h5>
                </button>
            </div>
            <div className={dataClass} >
                <div className="hospital-figs-wrp">
                    <h5 className="">Total Patients in Treatment - {treatment_total}</h5>
                    <h5 className="">Total Patients - {cumulative_total}</h5>
                    <div className="">Total Locals - {cumulative_local}</div>
                    <div className="">Total Foreigners - {cumulative_foreign}</div>
                    <div className="">Locals in Treatment - {treatment_local}</div>
                    <div className="">Foreigners in Treatment - {treatment_foreign}</div>
                </div>
            </div>
        </div>
    )
}

export default HospitalBlock;