

import React from 'react'
import ReactMinimalPieChart from 'react-minimal-pie-chart'


const PieChartBlock = (props) => {
    
    const dataSet = props.countryList;
    
    return (
        <div>
            <ReactMinimalPieChart
                animate={true}
                animationDuration={2000}
                animationEasing="ease-out"
                cx={50}
                cy={50}
                data={dataSet}
                label
                labelPosition={112}
                labelStyle={{
                    fontFamily: 'sans-serif',
                    fontSize: '2px'
                }}
                lengthAngle={360}
                lineWidth={45}
                paddingAngle={1}
                radius={35}
                rounded={false}
                startAngle={0}
                viewBoxSize={[
                    100,
                    100
                ]}
            />
        </div>
    )
}

export default PieChartBlock;