
import './ContactUs.css'
import React from 'react'
import ContactImage from '../../assets/images/contact-image.jpg'


const ContactUs = () => {
    return(
        <div>
             <h1 className="page-heading">Contact Info</h1>
            <div className="row ">
                <div className="col-md-6 personal-detail-wrp">
                    <h3 className="section-heading">The Developer</h3>
                    <p><strong>Name: </strong>Chaithika Abeywickrame</p>
                    <p><strong>Contact Number: </strong>+94 771557361</p>
                    <p><strong>LinkedIn: </strong><a href="https://www.linkedin.com/in/chaithika-abeywickrame-a03332105/">Chaithika Abeywickrame</a></p>
                    <p><strong>Company Name: </strong><a href="https://www.ism-apac.com/">ISM APAC</a></p>
                </div>
                <div className="col-md-6">
                    <img class="contact-image" src={ContactImage} alt="" srcset=""/>
                </div>
            </div>

        </div>
    )
}

export default ContactUs