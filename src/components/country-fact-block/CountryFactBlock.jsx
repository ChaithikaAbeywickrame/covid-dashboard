
import './CountryFactBlock.css'
import React from 'react'


const CountryFactBlock = (props) => {

    const customStyle = {
        color: props.color,
        border: `2px solid ${props.color}`,
    }

    return (
        <div className="country-figures-wrp" style={customStyle}>
            <strong>{props.title}</strong>
            <h4>{props.data}</h4>
        </div>
    )
}

export default CountryFactBlock;