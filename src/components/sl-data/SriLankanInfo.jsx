


import React from 'react';
import { useSelector, shallowEqual } from 'react-redux';

import DataTileBlock from '../data-tile-block/DataTileBlock'
import HospitalBlock from '../hospital-block/HospitalBlock'

const SriLankanInfo = () => {

    const { slFacts } = useSelector(state => ({
        slFacts: state.slData.srilankanData.data,
      }), shallowEqual);


    if (slFacts) {
        return (
            <div className="sl-content-wrp">
                <div className="world-summary-wrp">
                    <h1 className="page-heading">Sri Lankan Facts</h1>
                    <div className="summary-cases-wrp">
                        <h3 className="section-heading">Latest Updates as of - {slFacts.update_date_time}</h3>
                        <div className="row">
                            <DataTileBlock tileTitle='New Cases' tileContent={slFacts.local_new_cases} tileType='info' />
                            <DataTileBlock tileTitle='New Deaths' tileContent={slFacts.local_new_deaths} tileType='danger' />
                            <DataTileBlock tileTitle='Total Hospitalized' tileContent={slFacts.local_total_number_of_individuals_in_hospitals} tileType='dark' />
                            <DataTileBlock tileTitle='Total Cases' tileContent={slFacts.local_total_cases} tileType='primary' />
                            <DataTileBlock tileTitle='Total Deaths' tileContent={slFacts.local_deaths} tileType='danger' />
                            <DataTileBlock tileTitle='Total Recovered' tileContent={slFacts.local_recovered} tileType='success' />
                        </div>
                    </div>
                </div>
                <div className="hospital-summary-wrp">
                    <h3 className="section-heading">Status of Sri Lankan Hospitals</h3>
                    <div className="row">
                        {slFacts.hospital_data && slFacts.hospital_data.map((hospital, index) => (
                            <div className="col-md-6" key = {index}><HospitalBlock hospitalData = {hospital}/> </div>
                        ))
                        }
                    </div>
                </div>
            </div>
        )
    }
    return (
        <div className="custom-spinner text-center">
            <div className="spinner-border" role="status">
                <span className="sr-only">Loading...</span>
            </div>
        </div>
    )
}

export default SriLankanInfo;