import React, {useEffect} from 'react';
import './App.css';
import { useDispatch } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';

import {requestSummaryData} from './redux/actions/corona-data';
import {requestSrilankanData} from './redux/actions/sl-data';


import Dashboard from './containers/dashboard/Dashboard';
import Spinner from './components/spinner/Spinner';

function App() {
  const dispatch = useDispatch()
    

  useEffect(() => {
    dispatch(requestSummaryData())
    dispatch(requestSrilankanData())
    setInterval(() => {
      dispatch(requestSummaryData())
      dispatch(requestSrilankanData())
    }, 1000 * 1800);
  },[dispatch]);
  
  
  if((!dispatch(requestSummaryData())) && (!dispatch(requestSrilankanData))) {
    return (
      <Spinner/>
  )
  }
  return (
    <BrowserRouter>
      <Dashboard/>
    </BrowserRouter>
  );
}

export default App;
